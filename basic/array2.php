<?php 
$a[] = 1;
$a[5] = 2;
$a[] = 3;
$a["key"] = "value";

$b = array(1, 5 => 2, 3, "key" => "value");
?>
<table>
	<tr>
		<td>Key</td>
		<td>Value</td>
	</tr>
<?php 
	foreach ($a as $k => $v) {
		echo("<tr>
			<td>$k</td>
			<td>$v</td>
		</tr>");
	}
?>
</table>