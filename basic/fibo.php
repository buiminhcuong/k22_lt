<?php 
//Xây dựng hàm fibo nhận vào số n và trả về mảng gồm
//n số đầu tiên của dãy Fibonacci
//Ví dụ: n = 8
//[1, 1, 2, 3, 5, 8, 13, 21]
//Gọi hàm với n = 10, in mảng dưới dạng bảng
// STT | Gia tri
// 1   | 1
// 2   | 1
// 3   | 2
// 4   | 3
// ... | ...

//Tên hàm: fibo
//Đầu vào: n
//Đầu ra: Mảng fibo

function fibo($n) {
	$f = [1, 1];

	for($i = 2; $i < $n; $i++) {
		$f[$i] = $f[$i - 1] + $f[$i - 2];
	}

	return $f;
}

$f10 = fibo(10);

?>

<table>
	<tr>
		<td>STT</td>
		<td>Gia tri</td>
	</tr>
<?php 
	foreach ($f10 as $k => $v) {
		$no = $k + 1;
		echo("<tr>
			<td>{$no}</td>
			<td>$v</td>
		</tr>");
	}
?>
</table>