<?php 
$students = array(
	["id" => "A1234", "name" => "Nguyen Van An", "pob" => "Ha Noi"], 
	["id" => "A1235", "name" => "Nguyen Van Binh", "pob" => "Ha Noi"], 
	["id" => "A1236", "name" => "Nguyen Van Chung", "pob" => "Ha Noi"], 
	["id" => "A1237", "name" => "Nguyen Van Duong", "pob" => "Ha Noi"], 
	["id" => "A1238", "name" => "Nguyen Van Giang", "pob" => "Ha Noi"], 
	["id" => "A1239", "name" => "Nguyen Van Hai", "pob" => "Ha Noi"]);

//cho mảng 2 chiều students
//in ra danh sách dưới dạng bảng
//STT         | ID         |   Họ tên           |   Quê quán
//1           | A1234      |   Nguyễn Văn An    |   Ha Noi
//2           | A1235      |   Nguyen Van Binh  |   Ha Noi   
//...         | ...        |   ...              |   ...      
?>
<table>
	<tr>
		<td>STT</td>
		<td>ID</td>
		<td>Họ tên</td>
		<td>Quê quán</td>
	</tr>
<?php 
$stt=0;
foreach ($students as $student) { 
	$stt++;
?>
	<tr>
		<td><?=$stt?></td>
		<td><?=$student["id"]?></td>
		<td><?=$student["name"]?></td>
		<td><?=$student["pob"]?></td>
	</tr>
<?php } ?>
</table>