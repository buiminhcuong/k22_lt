<a href="cat_add.php">Add</a>
<?php 
$conn = mysqli_connect("localhost", "root", "", "k22") or die("Cannot connect to db" . mysqli_connect_error());
mysqli_set_charset($conn, "utf8");

$result = mysqli_query($conn, "SELECT * FROM cat");
if(mysqli_errno($conn) > 0) {
	die("Error execute query: " . mysqli_error($conn));
}

echo "<table>
		<tr>
			<td>Title</td>
			<td>Description</td>
			<td></td>
		</tr>";
while ($row = mysqli_fetch_assoc($result)) {
	echo("<tr>
			<td>{$row["title"]}</td>
			<td>{$row["description"]}</td>
			<td><a href=\"cat_edit.php?id={$row["id"]}\">Edit</a></td>
		</tr>");
}
echo "</table>";

mysqli_close($conn);

?>