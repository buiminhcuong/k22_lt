<?php 

function sum($a, $b = 20) {
	$s = $a + $b;

	return $s;
}

$s1 = sum(5, 10);
echo($s1);

$s2 = sum(8);
echo($s2);

//Xây dựng hàm age nhận vào năm sinh
//và trả về tuổi. Để lấy năm hiện tại
//sử dụng hàm date("Y")
//Đầu vào: năm sinh
//Tên hàm: age
//Đầu ra: tuổi
function age($yearOfBirth) {
	return date("Y") - $yearOfBirth;
}

echo("<br/>");
$age1 = age(1990);
echo("$age1 <br/>");
$age2 = age(1980);
echo("$age2 <br/>");
?>